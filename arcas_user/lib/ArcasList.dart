import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'FirebaseHelper.dart';
import 'SharedSearchFunctions.dart';
import 'SearchStateManager.dart';
import 'ArcasFilter.dart';

enum ArcasListState {
  waiting,
  loading,
  loaded
}

class _ListStateData {
  final Widget noResultsWidget; //A widget to show when no results are being shown
  final SearchButtonAppearance searchButtonAppearance; //A widget to show when no results are being shown

  _ListStateData({this.noResultsWidget, this.searchButtonAppearance});
}

class ArcasList extends StatefulWidget {
  @override
  _ArcasListState createState() => _ArcasListState();
}

class _ArcasListState extends State<ArcasList> {
  final ArcasSearchDelegate _searchDelegate = ArcasSearchDelegate();
  final SearchStateManager _searchStateManager = SearchStateManager();
  final Map<ArcasListState, _ListStateData> _states = {
    ArcasListState.waiting: _ListStateData(
      noResultsWidget: Center(
        child: Text(
          "Please use the search box at the top of the screen to search for services.",
          textAlign: TextAlign.center
        )
      ),
      searchButtonAppearance: SearchButtonAppearance.search
    ),
    ArcasListState.loading: _ListStateData(
      noResultsWidget: Center(
        child: Text(
          "Loading...",
          textAlign: TextAlign.center
        )
      ),
      searchButtonAppearance: SearchButtonAppearance.loading
    ),
    ArcasListState.loaded: _ListStateData(
      noResultsWidget: Center(
        child: Text(
          "No results found.",
          textAlign: TextAlign.center
        )
      ),
      searchButtonAppearance: SearchButtonAppearance.back
    )
  };

  _ListStateData _currentState;

  void _startSearch() {
    showSearch(context: context, delegate: _searchDelegate, query: _searchStateManager.query);
  }

  Expanded _buildSearchFieldText() {
    return Expanded(
      child: _searchStateManager.query == null || _searchStateManager.query == ""
        ? PlaceholderText()
        : ResultText(context, _searchStateManager.query)
    );
  }

  void _changeState(ArcasListState stateName) {
    setState(() => _currentState = _states[stateName]);
  }

  ArcasListState _determineState() {
    switch(_searchStateManager.state) {
      case SearchState.waiting:
        return ArcasListState.waiting;
      case SearchState.loading:
        return ArcasListState.loading;
      case SearchState.loaded:
      case SearchState.noResults:
      case SearchState.error:
        return ArcasListState.loaded;
      default:
        throw "Unimplemented 'SearchState' case";
    }
  }

  @override
  void setState(fn) { // Prevents off-screen setState() error
    if(mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();
    _currentState = _states[_determineState()];
    _searchStateManager.addListener(() => _changeState(_determineState()));
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> listSlivers = [
      SliverAppBar( //Custom AppBar
        forceElevated: true,
        pinned: true,
        leading: SearchButton(
          appearance: _currentState.searchButtonAppearance,
          searchOnTap: _startSearch,
          backOnTap: () => _searchStateManager.resetState()
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.filter_list,
              semanticLabel: "Filter"
            ),
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ArcasFilter()))
          )
        ],
        title: GestureDetector(
          behavior: HitTestBehavior.opaque,
          child: Row(
            children: [
              _buildSearchFieldText()
            ]
          ),
          onTap: () => _startSearch()
        )
      )
    ];

    if(_searchStateManager.locationList.isEmpty) {
      listSlivers.add(
        SliverFillRemaining(
          hasScrollBody: false,
          child: Container(
            padding: EdgeInsets.all(20),
            child: _currentState.noResultsWidget
          )
        )
      );
    }
    else {
      List<LocationPoint> locationPoints = _searchStateManager.locationList;
      listSlivers.add(
        SliverFillRemaining(
          child: ListView.separated(
            padding: EdgeInsets.only(top: 8),
            itemCount: locationPoints.length + 1, // + 1 to divider at bottom of list
            separatorBuilder: (_, __) => Divider(thickness: 1),
            itemBuilder: (context, index) {
              if(index == locationPoints.length) return Container(); // Add divider at bottom of list
              return ChangeNotifierProvider.value(
                value: locationPoints[index],
                child: Consumer<LocationPoint>(
                  builder: (context, locationPoint, child) {
                    return locationPoint.buildListTile(context);
                  }
                )
              );
            }
          )
        )
      );
    }

    return CustomScrollView(
      slivers: listSlivers
    );
  }
}