import 'package:flutter/material.dart';

// ColorScheme: https://api.flutter.dev/flutter/material/ColorScheme-class.html
// Color picker tool: https://material.io/design/color/the-color-system.html#tools-for-picking-colors

// From Flutter's theme spec: https://api.flutter.dev/flutter/material/ThemeData-class.html
// primaryColor: The background color for major parts of the app (toolbars, tab bars, etc).
// primaryColorDark: A darker version of the primaryColor.
// backgroundColor: A color that contrasts with the primaryColor, e.g. used as the remaining part of a progress bar.
// accentColor: The foreground color for widgets (knobs, text, overscroll edge effect, etc).
// iconTheme: An icon theme that contrasts with the card and canvas colors.

TextTheme lightText = Typography.material2018().black;
ThemeData themeLight = ThemeData.light().copyWith(
    primaryColor: Colors.white,
    backgroundColor: Colors.white,
    accentColor: Colors.blueAccent,
    scaffoldBackgroundColor: Colors.white,
    cardColor: Colors.white,
    buttonColor: Colors.blueAccent,
    toggleableActiveColor: Colors.blue,
    appBarTheme: AppBarTheme(
      elevation: 6.0
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: Colors.white,
      selectedItemColor: Colors.blueAccent,
      unselectedItemColor: Colors.black
    ),
    primaryTextTheme: lightText.copyWith(
      headline6: lightText.headline6.copyWith(fontSize: 20),
      caption: lightText.caption.copyWith(fontSize: 20),
    ),
    accentTextTheme: TextTheme(
      bodyText2: lightText.bodyText2.copyWith(color: Colors.white)
    ),
    primaryIconTheme: IconThemeData(
      color: Colors.black
    ),
    snackBarTheme: SnackBarThemeData(
        backgroundColor: Colors.white,
        contentTextStyle: lightText.bodyText2.copyWith(fontSize: 16.0)
    )
);

// Color(0xFF121212) is a Mat. Design recommended dark theme background colour
TextTheme darkText = Typography.material2018().white;
ThemeData themeDark = ThemeData.dark().copyWith(
    primaryColor: Colors.black,
    backgroundColor: Color(0xFF121212),
    accentColor: Colors.indigoAccent,
    scaffoldBackgroundColor: Colors.black,
    cardColor: Color(0xFF121212),
    buttonColor: Colors.indigoAccent,
    toggleableActiveColor: Colors.indigo,
    appBarTheme: AppBarTheme(
      elevation: 6.0
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: Colors.black,
      selectedItemColor: Colors.indigoAccent,
      unselectedItemColor: Colors.grey[300]
    ),
    primaryTextTheme: TextTheme(
        headline6: darkText.headline6.copyWith(color: Colors.grey[300], fontSize: 20),
        bodyText2: darkText.bodyText2.copyWith(color: Colors.grey[300])
    ),
    accentTextTheme: TextTheme(
        bodyText2: darkText.bodyText2.copyWith(color: Colors.black),
    ),
    primaryIconTheme: IconThemeData(
        color: Colors.grey[300]
    ),
    snackBarTheme: SnackBarThemeData(
        backgroundColor: Color(0xFF121212),
        contentTextStyle: darkText.bodyText2.copyWith(fontSize: 16.0)
    ),
);