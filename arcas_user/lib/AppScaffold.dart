import 'package:flutter/material.dart';

import 'ArcasMap.dart';
import 'ArcasList.dart';
import 'ArcasSaved.dart';
import 'ArcasSettings.dart';
import 'Themes.dart';
import 'ArcasGlobals.dart';

class AppScaffold extends StatefulWidget {
	final arcasScreens = [
		ArcasScreenData('Map', Icons.map, ArcasMap(), false),
		ArcasScreenData('List', Icons.view_list, ArcasList(), false),
		ArcasScreenData('Saved', Icons.bookmark_outline, ArcasSaved(), true),
		ArcasScreenData('Settings', Icons.settings, ArcasSettings(), true)
	];

	@override
	_AppScaffoldState createState() => _AppScaffoldState();
}

class _AppScaffoldState extends State<AppScaffold> {
	int _index = 0;
	final pageController = PageController();
	List<Widget> _screens;
	List<BottomNavigationBarItem> _tabItems;

	@override
  void initState() {
    super.initState();

		_screens = widget.arcasScreens.map((el) => el.widget).toList();
		_tabItems = widget.arcasScreens.map((el) =>
			BottomNavigationBarItem(label: el.label, icon: Icon(el.icon))
		).toList();

		// Arcas Welcome prompt
		// Only called after first frame of scaffold is rendered
		WidgetsBinding.instance.addPostFrameCallback((_) async {
			if(usersFirstVisit) {
				await showDialog(
					context: context,
					builder: (_) => AlertDialog(
						title: Text("Welcome to Arcas!"),
						content: Text(
							"Arcas aims to help those experiencing homelessness to find "
							"essential services.\n\n"
							"To get better search results, you can choose to enter your "
							"details in the settings page. We don't store your information. "
							"You can read more in the \"Data Usage\" section.",
						),
						actions: <Widget>[
							FlatButton(
								child: Text('Ok'),
								onPressed: () {
									Navigator.of(context).pop();
								}
							)
						]
					)
				);
			}
		});
  }

	@override
	Widget build(BuildContext context) {
		// Used for calculating appropriate shadow on dark theme
		Color bottomNavigationBarShadowColor = ElevationOverlay.applyOverlay(
				context,
				Theme.of(context).backgroundColor,
				Theme.of(context).appBarTheme.elevation
		);

		return Scaffold(
			appBar: widget.arcasScreens[_index].appBarVisible
				? AppBar(
					title: Text(widget.arcasScreens[_index].label)
				)
				: null,
			body: PageView( //required for ArcasMap/List persistence
				children: _screens,
				controller: pageController,
				physics: NeverScrollableScrollPhysics(), //prevent swipe navigation
			),
			bottomNavigationBar: Container(
				decoration: BoxDecoration(
					boxShadow: [
						BoxShadow( //to add correct shadow to bottom bar
							color: bottomNavigationBarShadowColor != themeLight.backgroundColor
								? bottomNavigationBarShadowColor
								: Colors.grey[300],
							blurRadius: 7
						)
					]
				),
			  child: BottomNavigationBar(
			  	type: BottomNavigationBarType.fixed,
			  	showSelectedLabels: true,
			  	showUnselectedLabels: true,
			  	currentIndex: _index,
			  	onTap: (tappedIndex) {
			  		pageController.jumpToPage(tappedIndex);
			  		setState(() {
			  			_index = tappedIndex;
			  		});
			  	},
			  	items: _tabItems
			  ),
			)
    );
	}
}

class ArcasScreenData {
	final String label;
	final IconData icon;
	final Widget widget;
	final bool appBarVisible;

	ArcasScreenData(this.label, this.icon, this.widget, this.appBarVisible);
}