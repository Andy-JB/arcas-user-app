# Arcas User App

Flutter app version of the Arcas user app
Note: Requires latest Flutter Beta (>= 1.22.0-12.1.pre)

## Preparing to run the App

- Checkout the branch you want to use.
- Go to pubspec.yaml and click "pub get" and "pub upgrade" to get the correct dependencies.
    - Ignore any warnings about them not being set.
- Make sure you're on the beta branch and have web enabled (to avoid errors)
	- `flutter channel beta`
	- `flutter upgrade`
	- `flutter config --enable-web`
- Navigate to root of project in CMD prompt
- Run `flutter create .` and delete "test/" folder
- Now you're ready to build the app for physical device, emulator or web.


## Notes

- No other notes.