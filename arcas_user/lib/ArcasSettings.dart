import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get/get.dart';
import 'package:geolocator/geolocator.dart'; // For opening location permissions

import 'SearchStateManager.dart';
import 'SubSettings/AboutArcas.dart';
import 'SubSettings/DataUsageInfo.dart';
import 'SharedPrefs.dart';
import 'Themes.dart' as myThemes;

class ArcasSettings extends StatefulWidget {
  @override
  _ArcasSettingsState createState() => _ArcasSettingsState();
}

class _ArcasSettingsState extends State<ArcasSettings> {
  AgeSettings ageSettings = AgeSettings();
  GenderSettings genderSettings = GenderSettings();

  bool isDarkSwitched = false;
  getDarkModePref() {
    isDarkSwitched = prefs.getDarkMode();
  }

  makeSubHeading(String title) {
    return Padding(
      padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
      child: Text(
        title,
        style: TextStyle(
          color: Theme.of(context).accentColor,
          fontWeight: FontWeight.bold,
          fontSize: 15.0,
        )
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    // List of icons
    // https://material.io/resources/icons/?icon=keyboard_arrow_right&style=baseline
    var renderedWidgetList = <Widget>[];
    var widgetList = <Widget>[
      makeSubHeading('Personal Details'),
      ListTile(
        title: Text('Age'),
        trailing: generateDropDown(ageSettings),
      ),
      ListTile(
        title: Text('Gender'),
        trailing: generateDropDown(genderSettings),
      ),
      ListTile(
        title: Text('Edit Location Permissions'),
        onTap: () {
          Geolocator.openAppSettings();
        },
      ),
      ListTile(
        title: Text('Reset Details',
          style: TextStyle(
            color: Colors.red,
            fontWeight: FontWeight.bold,
          ),
        ),
        onTap: () {
          showDialog(context: context, builder: (context) {
            return AlertDialog(
              title: Center(
                child: Text('Are you sure you want to delete your details?')
              ),
              actions: <Widget>[
                MaterialButton(
                  elevation: 5.0,
                  child: Text('No'),
                  onPressed: () => Navigator.pop(context)
                ),
                MaterialButton(
                  elevation: 5.0,
                  child: Text(
                    'Yes',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  color: Colors.red,
                  onPressed: () {
                    // Using setState will force the drop down widgets to rebuild
                    setState(() {
                      ageSettings.resetValue();
                      genderSettings.resetValue();
                    });
                    Navigator.pop(context);
                  }
                )
              ]
            );
          });
        }
      ),
      makeSubHeading('Legal'),
      ListTile(
        title: Text('Data Usage'),
        trailing: Icon(Icons.keyboard_arrow_right),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => DataUsageInfo()),
          );
        }
      ),
      makeSubHeading('Other'),
      ListTile(
        title: Text('About Arcas'),
        trailing: Icon(Icons.keyboard_arrow_right),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AboutArcas()),
          );
        }
      ),
      ListTile(
        title: Text('Services Australia Information'),
        trailing: Icon(Icons.keyboard_arrow_right),
        onTap: () {
          _launchURL('https://www.servicesaustralia.gov.au/individuals/topics/payment-and-service-finder/28656');
        }
      ),
      ListTile(
        title: Text('Dark Mode'),
        trailing: FutureBuilder(
          future: getDarkModePref(),
          builder: (BuildContext context, snapshot) {
            return Switch(
              value: isDarkSwitched,
              onChanged: (value) {
                setState(() {
                  isDarkSwitched = value;
                  Get.changeTheme(isDarkSwitched ? myThemes.themeDark : myThemes.themeLight);
                });
                prefs.setDarkMode(value);
              },
            );
          }
        )
      ),
    ];

    for(int i = 0; i < widgetList.length; i++) {
      renderedWidgetList.add(widgetList[i]);
      if(!(widgetList[i] is Padding) && i != widgetList.length - 1) {
        renderedWidgetList.add(Divider(thickness: 1));
      }
    }

    return SafeArea(
      child: DefaultTextStyle(
        style: Theme.of(context).primaryTextTheme.bodyText2,
        child: ListView(
            children: renderedWidgetList
        )
      )
    );
  }

  Future<void> _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget generateDropDown(DropSettings settings){
    return FutureBuilder(
      future: settings.getPref(),
      builder: (BuildContext context, snapshot) {
        return Container(width: 90,
          child: DropdownButton<String>(
            isExpanded: true,
            value: settings.chosen,
            icon: Icon(Icons.keyboard_arrow_down),
            iconSize: 24,
            elevation: 16,
            underline: Container(
              height: 2,
              color: Theme.of(context).accentColor,
            ),
            onChanged: (String newValue) async {
              setState(() {
                settings.chosen = newValue;
              });
              await settings.setPref();
            },
            items: settings.options.map<DropdownMenuItem<String>>((String value){
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          )
        );
      }
    );
  }
}

class DropSettings {
  List<String> get options => options;
  String get chosen => chosen;
  set chosen(String newChosen) => newChosen;

  DropSettings() {
    chosen = options.last;
  }

  void resetValue() {
    chosen = options.last;
    setPref();
  }

  // These will be re-defined in the inherited classes
  getPref(){}
  setPref(){}
}

class AgeSettings extends DropSettings {
  List<String> options = ['0 - 17', '18 - 25', '25+', 'NA'];
  String chosen = '';

  AgeSettings(): super();

  @override
  getPref() {
    chosen = prefs.getAge();
  }

  @override
  setPref() async {
    await prefs.setAge(chosen);
    SearchStateManager().resetFilters();
  }
}

class GenderSettings extends DropSettings {
  List<String> options = ['Male', 'Female', 'X', 'NA'];
  String chosen = '';

  GenderSettings(): super();

  @override
  getPref() {
    chosen = prefs.getGender();
  }

  @override
  setPref() async {
    await prefs.setGender(chosen);
    SearchStateManager().resetFilters();
  }
}
