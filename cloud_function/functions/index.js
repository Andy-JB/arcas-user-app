const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const stringify = require('json-stringify-safe');

// Firebase stores the category under the RHS names, but code uses LHS names
const categoryTranslate = {};
categoryTranslate['toilet'] = "Toilet";
categoryTranslate['food'] = "Food";
categoryTranslate['water'] = "Water";
categoryTranslate['emergency accommodation'] = "EmergencyAccommodation";
categoryTranslate['emergency relief'] = "EmergencyRelief";
categoryTranslate['charging point'] = "ChargingPoint";
categoryTranslate['transport'] = "Transport";
categoryTranslate['long term accommodation'] = "LongTermAccommodation";
categoryTranslate['shower'] = "Shower";
categoryTranslate['health service'] = "HealthService";
categoryTranslate['clothing'] = "Clothing";
categoryTranslate['community center'] = "CommunityCenter";
categoryTranslate['wifi'] = "WiFi";
categoryTranslate['laundry'] = "Laundry";
categoryTranslate['other'] = "Other";

const categories = ['toilet', 'food', 'water', 'emergency accommodation', 'emergency relief',
					'charging point', 'transport', 'long term accommodation', 'shower', 'health service',
					'clothing', 'community center', 'wifi', 'laundry', 'other'];
const wordLookup = new Map();

exports.searchServices = functions.https.onRequest(async (req, res) => {
	var query = "other"; // if no search terms, default to "other"
	try {
		query = req.query.query || req.body.data.query
	} catch(e) {console.log(e)}
	init();	// TODO: Separate this into a script that creates a database table so we don't have to regenerate this for every query
	const keywords = parse(query);
	const searchResults = await search(keywords);

	res.send(stringify({
		data: {
			message: `Query successful.`,
			keywords: keywords,
			searchResults: searchResults
		}
	}));
});

// Searches 'provider-services' collection for matches against the 'category' field
async function search(keywords) {
	const collection = admin.firestore().collection('provider-services');
	const serviceMatches = await collection.where('category', 'in', keywords).get();

	// Start location query queue
	const locationPromises = [];
	for(const serviceMatch of serviceMatches.docs) {
		const locationPromise = serviceMatch.data().location.get();
		locationPromises.push(locationPromise);
	}

	// Wait for all promises to resolve
	const locations = await Promise.all(locationPromises);

	// Filter out duplicate locations
	var uniqueLocations = [];
	locations.forEach((location) => {
		for(let i = 0; i < uniqueLocations.length; i++) {
			if(location.ref.id === uniqueLocations[i].ref.id) return;
		}
		uniqueLocations.push(location);
	});

	// Grab data of unique locations
	var results = uniqueLocations.map(location => {return {
		ref: location.ref.id,
		locationData: location.data()
	}});
	return results;
}

// Get the tags associated with the search query
function parse(query) {
	const ourCategories = new Set();
	const queryWords = query.toLowerCase().split(' ');

	queryWords.forEach(word => {
		var tags = wordLookup.get(word) || new Set();
		tags.forEach(tag => ourCategories.add(categoryTranslate[tag]));
	})

	if(ourCategories.size === 0) {
		ourCategories.add('other');
	}

	return Array.from(ourCategories);
}

// Add tags to a word to wordLookup
function add(word, tags, variant_mode) {
	const wordList = word.toLowerCase().split(' ');

	wordList.forEach(word => {
		// Get existing tags for word
		var currentTags = wordLookup.get(word) || new Set();

		// Add any new tags for word
		tags.forEach(category => currentTags.add(category));

		// TODO: Use variant_mode to find all variants and add them in

		wordLookup.set(word, currentTags);
	});
}

// Set up wordLookup
function init() {
	// Add direct matches to wordLookup, e.g. 'food' => ['food']
	categories.forEach(category => {
		var variant_mode = 1;
		if(category === 'other') {
			return;
		}
		else if(category === 'food' || category === 'emergency accommodation' || category === 'emergency relief' ||
			category === 'long term accommodation' || category === 'wifi' || category === 'laundry') {
			variant_mode = 0;
		}

		add(category, [category], variant_mode);
	});

	// Add close manual matches to wordLookup, e.g. 'dinner' => ['food']
	add('toilets', ['toilet'], 1);
	add('pee', ['toilet'], 1);
	add('poo', ['toilet'], 1);
	add('urinal', ['toilet'], 1);
	add('wash hand', ['toilet'], 1);

	add('eat', ['food'], 1);							// also eats/eatting/ate
	add('consume', ['food'], 1);
	add('hungry', ['food'], 1);
	add('edible', ['food'], 1);
	add('snack', ['food'], 1);
	add('breakfast', ['food'], 0);
	add('lunch', ['food'], 0);
	add('dinner', ['food'], 0);
	add('fill', ['food'], 1);							// filling

	add('soup', ['food', 'community center'], 1);
	add('taste', ['food', 'water'], 1);					// tasty
	add('meal', ['food', 'water'], 1);

	add('thirst', ['water'], 1);						// thirsty
	add('liquid', ['water'], 1);
	add('water', ['water'], 0);
	add('hydrate', ['water'], 1);						// hydration

	add('refresh', ['water', 'shower'], 1);				// refreshing

	add('bed', ['emergency accommodation'], 1);			// beds/bedding
	add('sleep', ['emergency accommodation'], 1);		// slept/sleeping
	add('snooze', ['emergency accommodation'], 1);
	add('short term', ['emergency accommodation'], 0);
	add('stay', ['emergency accommodation'], 1);			// stays/staying
	add('matress', ['emergency accommodation'], 1);		// matresses

	add('accommodation', ['emergency accommodation', 'long term accommodation'], 1);

	add('harm', ['emergency relief'], 0);
	add('bleed', ['emergency relief'], 1);				// bleeding/bled
	add('sick', ['emergency relief'], 0);
	add('cut', ['emergency relief'], 1);

	add('hospital', ['emergency relief', 'health service'], 1);
	add('health', ['emergency relief', 'health service'], 1);	// healthy
	add('hurt', ['emergency relief', 'health service'], 1);		// hurts/hurting
	add('medical', ['emergency relief', 'health service'], 1);	// medicine

	add('usb', ['charging point'], 0);
	add('cable', ['charging point'], 1);
	add('power', ['charging point'], 0);
	add('charge', ['charging point'], 1);				// charges/charging
	add('outlet', ['charging point'], 1);
	add('electricity', ['charging point'], 0);

	add('bus', ['transport'], 1);
	add('car', ['transport'], 1);
	add('bike', ['transport'], 1);
	add('wheel', ['transport'], 1);
	add('train', ['transport'], 1);
	add('tram', ['transport'], 1);
	add('taxi', ['transport'], 1);

	add('home', ['long term accommodation'], 1);			// house
	add('appartment', ['long term accommodation'], 1);
	add('live', ['long term accommodation'], 1);			// lives/living
	add('stay', ['long term accommodation'], 1);

	add('wash body', ['shower'], 0);
	add('clean', ['shower'], 1);						// cleanse
	add('sponge', ['shower'], 1);
	add('soap', ['shower'], 1);
	add('shampoo', ['shower'], 1);

	add('dr', ['health service'], 1);
	add('doctor', ['health service'], 1);
	add('pharmacy', ['health service'], 1);				// pharmacist
	add('drug', ['health service'], 1);
	add('sore', ['health service'], 0);
	add('sick', ['health service'], 0);
	add('tablet', ['health service'], 1);
	add('emergency', ['health service'], 1);

	add('wear', ['clothing'], 0);
	add('blanket', ['clothing'], 1);
	add('cloth', ['clothing'], 0);
	add('warm', ['clothing'], 1);						// warmth/warms
	add('beenie', ['clothing'], 1);
	add('hat', ['clothing'], 1);
	add('t-shirt', ['clothing'], 1);					// t shirt
	add('short', ['clothing'], 1);
	add('pants', ['clothing'], 0);
	add('sock', ['clothing'], 1);
	add('underwear', ['clothing'], 0);
	add('underpants', ['clothing'], 0);
	add('jocks', ['clothing'], 0);
	add('bra', ['clothing'], 1);
	add('shirt', ['clothing'], 1);
	add('dress', ['clothing'], 1);
	add('skirt', ['clothing'], 1);
	add('shoe', ['clothing'], 1);
	add('glasses', ['clothing'], 0);

	add('meet', ['community center'], 1);
	add('greet', ['community center'], 1);
	add('friend', ['community center'], 1);
	add('talk', ['community center'], 1);				// talks/talking
	add('chat', ['community center'], 1);
	add('game', ['community center'], 1);
	add('play', ['community center'], 1);				// plays/playing
	add('playcenter', ['community center'], 1);
	add('park', ['community center'], 1);				// parks/parking
	add('course', ['community center'], 1);
	add('class', ['community center'], 1);
	add('church', ['community center'], 1);
	add('worship', ['community center'], 1);
	add('mosque', ['community center'], 1);
	add('religion', ['community center'], 1);			// religious
	add('library', ['community center'], 1);
	add('gym', ['community center'], 1);

	add('internet', ['wifi'], 1);
	add('google', ['wifi'], 1);
	add('search', ['wifi'], 1);							// searches/searching
	add('signal', ['wifi'], 1);

	add('wash clothes', ['laundry'], 0);
	add('washing machine', ['laundry'], 1);
	add('wash', ['laundry'], 1);						// washes/washing
	add('dry', ['laundry'], 1);							// drys/dryer/drying
	add('clothes', ['laundry'], 1);						// clothing
	add('clean', ['laundry'], 1);						// cleans/cleaning
	add('laundrette', ['laundry'], 1);
	add('iron', ['laundry'], 1);						// ironer/ironing
}
