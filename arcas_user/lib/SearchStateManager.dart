import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'dart:async';

import 'FirebaseHelper.dart';
import 'ArcasFilter.dart';

enum SearchState {
  waiting,
  loading,
  loaded,
  noResults,
  error
}

// Singleton class (all initialisations of this class refer to the same
// instance)
// Holds the current query and locations the app should be displaying
// Should be listened to for updates.
class SearchStateManager extends ChangeNotifier {
  // Singleton boilerplate
  SearchStateManager._internal();
  static final SearchStateManager _singleton = SearchStateManager._internal();
  factory SearchStateManager() => _singleton;

  final HttpsCallable _searchServices = firebaseHelper.functions.httpsCallable('searchServices');

  SearchState state = SearchState.waiting;
  String query = "";
  StreamSubscription<HttpsCallableResult<dynamic>> currentStream;
  //locationList is viewed by apps, waitingList have not been checked by filter yet, filteredList were locations that currently don't meet filter requirements
  List<LocationPoint> waitingList = [];
  List<LocationPoint> locationList = [];  // Stuff that we're keeping
  List<LocationPoint> filteredList = [];  // Stuff that has been filtered *out*

  void cancelSearch() {
    currentStream?.cancel();
  }

  void _updateState(SearchState newState) {
    state = newState;
    notifyListeners();
  }

  void resetState() {
    cancelSearch();
    query = "";
    clearLists();
    _updateState(SearchState.waiting);
  }

  void _processResponse(HttpsCallableResult<dynamic> response) {
    cancelSearch();
    clearLists();

    List<Map>.from(response.data["searchResults"])
    ..removeWhere((el) => el["locationData"] == null)
    ..forEach((location) => waitingList.add(LocationPoint(location)));

    _getServicesInWaitingList();
  }

  void _getServicesInWaitingList() {
    if(waitingList.isEmpty) return _updateState(SearchState.noResults);

      waitingList.forEach((location) {
        location.serviceStream = firebaseHelper.getServicesFromId(location.id).listen((querySnapshot) {
          location.updateServicesList(querySnapshot.docChanges);
        });
      });
  }

  void clearLists() {
    for(LocationPoint location in [...locationList, ...filteredList, ...waitingList].toSet().toList()) {
        location.removeListener(location.updateSymbol);
        location.cancelStream();
      }
    locationList.clear();
    filteredList.clear();
    waitingList.clear();
  }

  void resetFilters() {
    waitingList = [...locationList, ...filteredList, ...waitingList].toSet().toList();
    while(waitingList.isNotEmpty) {
      processFilter(waitingList[0]);
    }
  }

  void processFilter(LocationPoint location) {
    locationList.remove(location);
    filteredList.remove(location);
    location.updateCapacities();
    if(filterStorage.filterLocation(location)) {  // Checks if the location has a valid service
      locationList.add(location);
    }
    else {
      filteredList.add(location);
    }
    waitingList.remove(location);
    if(waitingList.isEmpty) {
      _updateState(
        locationList.isEmpty ? SearchState.noResults : SearchState.loaded
      );
    }
  }

  void newSearch(String newQuery) async {
    if(newQuery == "") return resetState();

    query = newQuery;
    _updateState(SearchState.loading);

    currentStream = _searchServices.call(<String, String>{"query": newQuery}).asStream().listen(_processResponse);
  }
}