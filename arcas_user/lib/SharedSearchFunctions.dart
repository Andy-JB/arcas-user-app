import 'package:flutter/material.dart';

import 'SearchStateManager.dart';
import 'SharedPrefs.dart';
import 'ArcasFilter.dart';

class ArcasSearchDelegate extends SearchDelegate<void> {
  final SearchStateManager _searchStateManager = SearchStateManager();
  @override TextStyle searchFieldStyle = TextStyle(color: Colors.grey, fontSize: 20.0);

  @override
  ThemeData appBarTheme(context) => Theme.of(context); // Force correct theme

  void _saveToRecentSearches()  {
    if(query == null || query == "") return;

    List<String> allSearches = prefs.getRecentSearches()
      ..remove(query)
      ..insert(0, query); // Insert at top of list

    prefs.setRecentSearches(allSearches); // No need to await; does not depend on result
  }

  List<String> _getSimilarRecentSearches() {
    return prefs.getRecentSearches().where((search) => search.startsWith(query)).toList();
  }

  void _sendSearch(BuildContext context) {
    _searchStateManager.newSearch(query);
    _saveToRecentSearches();
    close(context, null);
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      query != ""
        ? IconButton(
            icon: Icon(Icons.clear), //Clears search box
            onPressed: () {
              query = "";
            }
          )
        : Container(),
      IconButton(
        icon: Icon(Icons.filter_list), //Pops out filter menu
        onPressed: () {
          //Toggle filter menu here
          Navigator.push(context, MaterialPageRoute(builder: (context) => ArcasFilter()));
        }
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton( //Back button
      icon: Icon(Icons.arrow_back),
      onPressed: () => close(context, null)
    );
  }

  // Returns immediately, passing the search responsibility to SearchStateManager
  @override
  Widget buildResults(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => _sendSearch(context));
    return Center(child: Text("Loading...")); // Should never show
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return LayoutBuilder( // Allows scrolling of list when keyboard is showing
      builder: (context, viewportConstraints) {
        return ConstrainedBox(
          constraints: BoxConstraints(minHeight: viewportConstraints.maxHeight),
          child: IntrinsicHeight(
            child: StatefulBuilder(builder: (context, setState) {
              List<String> suggestionList = _getSimilarRecentSearches();

              // Row that shows if recent searches have been found
              EdgeInsets rowChildTBPadding = EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 10.0);
              Widget topTextRow = Padding(
                padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                child: Row(
                  children: [
                    Padding(
                      padding: rowChildTBPadding,
                      child: Text(
                        "Recent Searches",
                        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16.0)
                      )
                    ),
                    Spacer(),
                    GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      child: Container( // Forces taps on text's padding to be registered
                        padding: rowChildTBPadding,
                        child: Text(
                          "Clear recent",
                          style: TextStyle(color: Colors.grey)
                        )
                      ),
                      onTap: () async {
                        await prefs.setRecentSearches([]);
                        setState((){});
                      }
                    )
                  ]
                )
              );

              // Row that shows if no recent searches have been found, or
              // no recent searches are similar to the current query
              if(suggestionList.isEmpty) {
                String noResultsText = query == ""
                                      ? "No recent searches."
                                      : "No similar recent searches.";
                topTextRow = Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Text(noResultsText, textAlign: TextAlign.center)
                    )
                  ]
                );
              }

              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  topTextRow,
                  Expanded(
                    child: Container(
                      height: 100.0, //arbitrary; required as a min height
                      child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: suggestionList.length,
                        itemBuilder: (context, index) {
                          return ListTile(
                            title: Text(suggestionList[index]),
                            leading: Icon(Icons.restore),
                            onTap: () {
                              query = suggestionList[index];
                              showResults(context);
                            }
                          );
                        }
                      )
                    )
                  )
                ]
              );
            })
          )
        );
      }
    );
  }
}

// PlaceholderText and ResultText are used by SearchBox and List view to
// build appropriately styled contents for their search fields.
abstract class SearchFieldText extends Text {
  SearchFieldText(String data, Color color):super(
    data,
    textAlign: TextAlign.left,
    overflow: TextOverflow.ellipsis,
    maxLines: 1,
    style: TextStyle(
      fontSize: 20.0,
      fontWeight: FontWeight.normal,
      color: color
    )
  );
}

class PlaceholderText extends SearchFieldText {
  PlaceholderText():super("Search", Colors.grey);
}

class ResultText extends SearchFieldText {
  ResultText(BuildContext context, String data):super(data, null);
}

// SearchButton is used by SearchBox and List view to indicate tbe state of the
// search to the user.
enum SearchButtonAppearance {
  search,
  loading,
  back
}

class SearchButtonAppearanceData {
  final IconData iconData;
  final String semanticText;
  final Function onIconTap;
  SearchButtonAppearanceData(this.iconData, this.semanticText, this.onIconTap);
}

class SearchButton extends StatelessWidget {
  final double iconSize;
  final SearchButtonAppearance appearance;
  final Function searchOnTap;
  final Function loadingOnTap;
  final Function backOnTap;
  final EdgeInsets padding;

  SearchButton({
    this.iconSize = 24.0,
    this.appearance = SearchButtonAppearance.search,
    this.searchOnTap,
    this.loadingOnTap,
    this.backOnTap,
    this.padding = const EdgeInsets.all(8.0)
  });

  @override
  Widget build(BuildContext context) {
    SearchButtonAppearanceData searchButtonData;
    switch(appearance) {
      case SearchButtonAppearance.search: searchButtonData = SearchButtonAppearanceData(Icons.search, "Search icon", searchOnTap);
      break;
      case SearchButtonAppearance.loading: searchButtonData = SearchButtonAppearanceData(null, "Loading data", loadingOnTap);
      break;
      case SearchButtonAppearance.back: searchButtonData = SearchButtonAppearanceData(Icons.arrow_back, "Back icon", backOnTap);
      break;
    }

    Color colour = Theme.of(context).primaryIconTheme.color;
    Widget icon;
    if(searchButtonData.iconData == null) { // Create loading icon
      icon = SizedBox(
        width: iconSize,
        height: iconSize,
        child: CircularProgressIndicator(
          strokeWidth: 2.0,
          valueColor: AlwaysStoppedAnimation<Color>(colour)
        )
      );
    } else { // Create search or back button icon
      icon = Icon(
        searchButtonData.iconData,
        size: iconSize,
        semanticLabel: searchButtonData.semanticText,
        color: colour,
      );
    }

    return IconButton(
      padding: padding,
      icon: icon,
      onPressed: searchButtonData.onIconTap
    );
  }
}