import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:provider/provider.dart';
import 'package:geolocator/geolocator.dart'; // For getting the user's location
import 'package:url_launcher/url_launcher.dart';

import 'ArcasSaved.dart';
import 'FirebaseHelper.dart';
import 'ArcasConstants.dart';

class ArcasLocationPage extends StatefulWidget {
  static LocationPoint lp;
  static String today;
  static List<String> dates;

  ArcasLocationPage(LocationPoint data) {
    lp = data;
  }

  static void assembleDayList() {
    List<String> ret = [];
    var time = DateTime.now();
    var today = DateFormat('EEEE').format(time);
    ret.add(today + " (Today)");
    today = ret[0];
    for (int i = 1; i < 7; ++i) {
      time = time.add(Duration(days: 1));
      ret.add( DateFormat('EEEE').format(time) + " ($i day${i == 1 ? "" : "s"})");
    }
    dates = ret;
  }

  static Route createRoute(LocationPoint locationPoint) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => ArcasLocationPage(locationPoint),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  @override
  _ArcasLocationPageState createState() => _ArcasLocationPageState();
}

class _ArcasLocationPageState extends State<ArcasLocationPage> {
  final ArcasSavedManager _arcasSavedManager = ArcasSavedManager();
  final ScrollController _controllerVertical = ScrollController();
  final ScrollController _controllerHorizontal = ScrollController(keepScrollOffset: false);
  final double _iconSize = 32.0;

  List<Widget> _infoList;
  String _dropdownValue;
  SaveButtonState _currentSaveButtonState;
  Map<SaveButtonState, SaveButtonStateData> _saveButtonStates;

  Future<void> _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    super.initState();

    ArcasLocationPage.assembleDayList();
    _dropdownValue = ArcasLocationPage.dates[0];

    _saveButtonStates = {
      SaveButtonState.notSaved: SaveButtonStateData(Icon(Icons.bookmark_outline, size: _iconSize), "Save", () async {
        setState(() => _currentSaveButtonState = SaveButtonState.loading);
        await _arcasSavedManager.saveLocation(ArcasLocationPage.lp);
        setState(() => _currentSaveButtonState = SaveButtonState.saved);
      }),
      SaveButtonState.loading: SaveButtonStateData(
          Center(child: SizedBox(
                  // Loading animation
                  width: _iconSize * 0.8,
                  height: _iconSize * 0.8,
                  child: CircularProgressIndicator(strokeWidth: 2.0))),
          "Save",
          null),
      SaveButtonState.saved: SaveButtonStateData(Icon(Icons.bookmark_outlined, size: _iconSize), "Saved", () async {
        await _arcasSavedManager.unsaveLocation(ArcasLocationPage.lp);
        setState(() => _currentSaveButtonState = SaveButtonState.notSaved);
      })
    };

    _currentSaveButtonState = _arcasSavedManager.isSaved(ArcasLocationPage.lp) ? SaveButtonState.saved : SaveButtonState.notSaved;
  }

  Column createUserButton(Widget iconWidget, String flavorText, Function onTap) {
    Color colour = Theme.of(context).accentColor;
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          FittedBox(
            fit: BoxFit.contain,
            child: Container(
                padding: const EdgeInsets.all(0.0),
                foregroundDecoration: BoxDecoration(
                  border: Border.all(
                    color: colour,
                    width: 2,
                  ),
                  shape: BoxShape.circle,
                ),
                child: SizedBox(
                  width: _iconSize * 1.5,
                  height: _iconSize * 1.5,
                  child: IconButton(
                      padding: EdgeInsets.all(0),
                      color: colour,
                      iconSize: _iconSize * 0.8,
                      icon: iconWidget,
                      alignment: Alignment.center,
                      onPressed: () => onTap?.call()), //.call for null-safety
                )),
          ),
          Text(flavorText, style: TextStyle(height: 1.5))
        ]);
  }

  Column createSaveButton() {
    SaveButtonStateData state = _saveButtonStates[_currentSaveButtonState];
    return createUserButton(state.iconData, state.text, state.onTap);
  }

  //Checks if a button will be useful before generating it
  List<Widget> createButtons(LocationPoint location) {
    List<Widget> buttons = [];

    buttons.add(createUserButton(
        Icon(Icons.directions, size: _iconSize),
        "Directions",
        () => _launchURL("https://www.google.com/maps/search/?api=1&query=" +
            location.latLng['lat'].toString() + "," + location.latLng['lng'].toString())));

    if (location.phoneNumber != "")
      buttons.add(createUserButton(Icon(Icons.call, size: _iconSize), "Call",
          () => _launchURL("tel:" + location.phoneNumber)));

    buttons.add(createUserButton(
        Icon(Icons.sentiment_satisfied, size: _iconSize),
        "Feedback",
        () => showDialog(
            context: context,
            builder: (BuildContext context) {
              double ratingStars;
              String ratingComeBack;
              String ratingFeedback = "";
              bool ratingError = false;
              return StatefulBuilder(
                  builder: (context, setState) {
                return AlertDialog(
                  content: SingleChildScrollView(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(children: [
                              Text("Rate This Service"),
                              RatingBar.builder(
                                minRating: 1,
                                allowHalfRating: false,
                                itemCount: 5,
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (double rating) {
                                  setState(() => ratingStars = rating);
                                },
                              ),
                            ],
                          ),
                          ratingError && ratingStars == null ? Text("Please rate this service", style: TextStyle(color: Colors.red)) : SizedBox(height: 10),
                          Column(children: [
                              Text(
                                "Would you come back?",
                              ),
                              RadioListTile(
                                  title: Text("Yes"),
                                  value: "Yes",
                                  groupValue: ratingComeBack,
                                  onChanged: (String value) {
                                    setState(() {
                                      ratingComeBack = value;
                                    });
                                  }),
                              RadioListTile(
                                  title: Text("Not Sure"),
                                  value: "Not Sure",
                                  groupValue: ratingComeBack,
                                  onChanged: (String value) {
                                    setState(() {
                                      ratingComeBack = value;
                                    });
                                  }),
                              RadioListTile(
                                  title: Text("No"),
                                  value: "No",
                                  groupValue: ratingComeBack,
                                  subtitle: ratingError && ratingComeBack == null ? Text("Please select Yes, Not Sure or No.", style: TextStyle(color: Colors.red)) : null,
                                  onChanged: (String value) {
                                    setState(() {
                                      ratingComeBack = value;
                                    });
                                  }),
                            ],
                          ),
                          //todo: make TextField look better (Probably don't make box disappear on tap)
                          TextField(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'How was your experience?'),
                            onSubmitted: (String submit) {
                              ratingFeedback = submit;
                            },
                          )
                        ]),
                  ),
                  actions: <Widget>[
                    TextButton(
                      child: Text("Cancel"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    TextButton(
                        child: Text("Submit"),
                        onPressed: () {
                          if (ratingStars != null && ratingComeBack != null && ratingFeedback != null)  {
                            firebaseHelper.sendFeedback(ratingStars.floor(), ratingComeBack, ratingFeedback, location.id);
                            Navigator.of(context).pop();
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text("Thank you for your feedback!")));
                          }
                          else {
                            setState(() => ratingError = true);
                          }
                        }),
                  ],
                );
              });
            })));

    if (location.website != "")
      buttons.add(createUserButton(Icon(Icons.link, size: _iconSize), "Website",
          () => _launchURL(location.website)));

    buttons.add(createSaveButton());

    return buttons;
  }

  setInformation(LocationPoint location) {
    TextStyle textStyle = Theme.of(context).textTheme.bodyText2.copyWith(height: 1.5);

    Widget img = location.image != ""
        ? Container(
            height: 150,
            child: CachedNetworkImage(
              imageUrl: location.locationInfo["orgAvatar"],
              placeholder: (context, url) => Transform.scale(
                  scale: 0.33,
                  child: FittedBox(
                      fit: BoxFit.contain,
                      child: CircularProgressIndicator(strokeWidth: 1))),
              errorWidget: (context, url, error) => Icon(Icons.error),
              fit: BoxFit.contain,
            ))
        : Text("No Location Image",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 16, height: 2, fontWeight: FontWeight.bold));

    _infoList = [
      img,
      Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 4),
          child: Text(location.name,
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
              )),
        ),
        Text(location.address),
        location.buildOpeningTimes(),
        FutureBuilder(
            future: Geolocator.checkPermission(),
            builder: (context, snapshot) {
              // If its finished loading and the user enabled tracking
              if (snapshot.connectionState == ConnectionState.done &&
                  snapshot.data != LocationPermission.denied &&
                  snapshot.data != LocationPermission.deniedForever) {
                return FutureBuilder(
                    future: Geolocator
                        .getLastKnownPosition(), // Safer if GPS is off.
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.data != null) {
                        double distance = Geolocator.distanceBetween(
                            location.latLng["lat"],
                            location.latLng["lng"],
                            snapshot.data.latitude,
                            snapshot.data.longitude); // This is in meters
                        String unit;
                        if (distance < 1000) {
                          unit = 'm';
                        } else {
                          unit = 'km';
                          distance /= 1000;
                        }
                        return Text(
                          'Distance: ' +
                              distance.toStringAsFixed((unit == 'm') ? 0 : 1) +
                              unit,
                          style: textStyle,
                        );
                      }
                      return Container();
                    });
              }
              return Container();
            }),
        Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Text(
              location.description,
              style: textStyle,
            ))
      ]),
      //TODO: Make image size non-fixed (Should be based on width of page)
      Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: createButtons(location)),
      Row(children: <Widget>[
        Text("Services Offered For  ",
            style: TextStyle(fontWeight: FontWeight.bold)),
        DropdownButton<String>(
          onChanged: (String newValue) {
            setState(() {
              _dropdownValue = newValue;
              _controllerHorizontal.animateTo(0,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.fastOutSlowIn);
            });
          },
          items: ArcasLocationPage.dates
              .map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
          value: _dropdownValue,
        )
      ]),
      //height must be a value or it won't render
      Consumer<LocationPoint>(
        builder: (context, locationPoint, child) {
          List<Widget> serviceWidgetList = createServicesView(
              location.serviceList,
              _dropdownValue.substring(0, _dropdownValue.indexOf(" ")),
              _dropdownValue == ArcasLocationPage.dates[0]);
          return Container(
            height: 300,
            child: NotificationListener<ScrollNotification>(
              onNotification: (notification) => true,
              child: Scrollbar(
                isAlwaysShown: serviceWidgetList.length > 2,
                controller: _controllerHorizontal,
                thickness: 4,
                child: GridView.count(
                  crossAxisCount: 2,
                  scrollDirection: Axis.horizontal,
                  childAspectRatio: 4.0 / 9.0,
                  controller: _controllerHorizontal,
                  children: serviceWidgetList,
                ),
              ),
            ),
          );
        },
      ),
    ];
  }

  List<Widget> createServicesView(Set<ArcasService> services, String day, bool showCapacity) {
    List<Widget> widgetList = [];
    for (var service in services) {
      var serviceDay = service.openingHours.day(day);
      if (serviceDay.isOpen == false || service.enabled == false) {
        continue;
      }

      double textPadding = 10;
      widgetList.add(IntrinsicWidth(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(children: [
              FittedBox(
                  child: Icon(ArcasConstants.iconNames[service.category][0],
                      size: 50)),
              SizedBox(width: textPadding + 3.0),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text(service.category,
                    style: TextStyle(fontWeight: FontWeight.bold, height: 2),
                    textAlign: TextAlign.left),
                Text("Genders: ${genderAgeTextGen(service.genders)}",
                    textAlign: TextAlign.left),
                Text("Ages: ${genderAgeTextGen(service.ages)}",
                    textAlign: TextAlign.left),
                Text(
                    //capitalises first letter and lowercase other letters (Capacities are FULL CAPS in database)
                    showCapacity
                        ? "Capacity: ${service.capacity[0] + (service.capacity == "NA" ? "\/A" : service.capacity.substring(1).toLowerCase())}"
                        : "")
              ])
            ]),
            Row(children: [
              FittedBox(
                  child: Icon(ArcasConstants.iconNames['Clock'][0], size: 50)),
              SizedBox(width: textPadding),
              Text(getServiceTime(serviceDay.times))
            ])
          ],
        ),
      ));
    }
    if (widgetList.length == 0) {
      return [Text("This day has no available services")];
    }
    return widgetList;
  }

  //arr is at max length three
  String genderAgeTextGen(List<dynamic> arr) {
    //translating firebase text to real text
    Map<dynamic, String> gendersAges = {
      GenderOptions.F: "Female",
      GenderOptions.M: "Male",
      GenderOptions.X: "X",
      AgeOptions.A250: "Over 25",
      AgeOptions.A1825: "Between 18-25",
      AgeOptions.AU18: "Under 18"
    };
    if (arr.length == 3) {
      return "All";
    } else if (arr.length == 2) {
      return "${gendersAges[arr[0]]} and ${gendersAges[arr[1]]}";
    } else if (arr.length == 1) {
      return "${gendersAges[arr[0]]}";
    } else
      return "Error!";
  }

  String getServiceTime(List<OpeningHoursDayHours> times) {
    String ret = "";
    for (int i = 0; i < times.length; ++i) {
      if (i > 0) {
        ret += "\n";
      }
      ret +=
          " ${timeToAmPm(times[i].startTime)} - ${timeToAmPm(times[i].endTime)}";
    }
    return ret;
  }

  String timeToAmPm(String date) {
    final DateFormat formatter = DateFormat('jm');
    DateTime tempDate = new DateFormat("Hm").parse(date);
    return formatter.format(tempDate);
  }

  @override
  Widget build(BuildContext context) {
    setInformation(ArcasLocationPage.lp);
    return ChangeNotifierProvider.value(
      value: ArcasLocationPage.lp,
      child: Scaffold(
        appBar: AppBar(title: Text('View Location')),
        body: Scrollbar(
          isAlwaysShown: true,
          controller: _controllerVertical,
          thickness: 4,
          child: ListView.builder(
              itemCount: _infoList.length,
              padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
              controller: _controllerVertical,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                    clipBehavior: Clip.antiAlias,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: _infoList[index],
                    ));
              }),
        ),
      ),
    );
  }
}

enum SaveButtonState { notSaved, loading, saved }

class SaveButtonStateData {
  Widget iconData;
  String text;
  Future<void> Function() onTap;

  SaveButtonStateData(this.iconData, this.text, this.onTap);
}
