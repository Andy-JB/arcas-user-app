import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'SharedPrefs.dart';
import 'FirebaseHelper.dart';

// Singleton class (all initialisations of this class refer to the same
// instance)
// Holds the LocationPoints for saved 'provider-location' documents.
//
// When first initialised, it retrieves saved document IDs from
// shared_preferences and asks Firestore for the DocumentSnapshot for each.
// These are converted to LocationPoints and stored in savedLocations.
//
// To-be-saved documents are retrieved, converted and stored on press of a
// 'Save' button on the LocationInformation page, and their IDs are saved to
// shared_preferences for the next app startup.
enum ArcasSavedManagerState {
  noneSaved,
  loading,
  loaded
}

class ArcasSavedManager extends ChangeNotifier {
  // Singleton boilerplate
  ArcasSavedManager._internal() {
    _getInitialSaved(); //Check current shared_pref storage
  }
  static final ArcasSavedManager _singleton = ArcasSavedManager._internal();
  factory ArcasSavedManager() => _singleton;

  ArcasSavedManagerState state;
  List<LocationPoint> savedLocations;
  bool needsUpdate = false;

  void _changeState(ArcasSavedManagerState newState) {
    if(newState == ArcasSavedManagerState.loaded &&
        prefs.getSavedLocationIDs().isEmpty) {
      newState = ArcasSavedManagerState.noneSaved;
    }
    state = newState;
    notifyListeners();
  }

  void _getInitialSaved() async {
    _changeState(ArcasSavedManagerState.loading);

    await firebaseHelper.getMatchingDocuments('provider-location', prefs.getSavedLocationIDs())
        .then((data) {
      savedLocations = data.map((doc) => LocationPoint.fromDocumentSnapshot(doc)).toList();
      _changeState(ArcasSavedManagerState.loaded);
    });
  }

  // Updates the relevant shared_preferences field
  Future<void> _processLocationIDsUpdate(Function listOperation) async {
    List<String> savedLocations = prefs.getSavedLocationIDs();
    listOperation(savedLocations);
    await prefs.setSavedLocationIDs(savedLocations);
    _changeState(ArcasSavedManagerState.loaded);
  }

  Future<void> saveLocation(LocationPoint location) async {
    _changeState(ArcasSavedManagerState.loading);
    savedLocations.add(LocationPoint.fromDocumentSnapshot(await firebaseHelper.getDoc('provider-location', location.id)));
    await _processLocationIDsUpdate((list) => list.add(location.id));
  }

  Future<void> unsaveLocation(LocationPoint location) async {
    _changeState(ArcasSavedManagerState.loading);
    savedLocations.removeWhere((element) => element.id == location.id);
    await _processLocationIDsUpdate((list) => list.remove(location.id));
  }

  bool isSaved(LocationPoint location) => prefs.getSavedLocationIDs().contains(location.id);
}



class ArcasSaved extends StatefulWidget {
  @override
  _ArcasSavedState createState() => _ArcasSavedState();
}

class _ArcasSavedState extends State<ArcasSaved> {
  final ArcasSavedManager _arcasSavedManager = ArcasSavedManager();

  @override
  void setState(fn) { // Prevents off-screen setState() error
    if(mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();
    _arcasSavedManager.addListener(() => setState((){}));
  }

  @override
  Widget build(BuildContext context) {
    switch(_arcasSavedManager.state) {
      case ArcasSavedManagerState.loading:
        return Center(
          child: SizedBox( // Loading
            width: 36.0,
            height: 36.0,
            child: CircularProgressIndicator(strokeWidth: 2.0)
          )
        );

      case ArcasSavedManagerState.loaded:
        List<LocationPoint> locationPoints = _arcasSavedManager.savedLocations;
        return ListView.separated(
          padding: EdgeInsets.only(top: 8),
          itemCount: locationPoints.length + 1, // + 1 to divider at bottom of list
          separatorBuilder: (_, __) => Divider(thickness: 1),
          itemBuilder: (context, index) {
            if(index == locationPoints.length) return Container(); // Add divider at bottom of list
            return ChangeNotifierProvider.value(
              value: locationPoints[index],
              child: Consumer<LocationPoint>(
                builder: (context, locationPoint, child) {
                  return locationPoint.buildListTile(context);
                }
              )
            );
          }
        );

      case ArcasSavedManagerState.noneSaved:
      default:
        return Center(
          child: Text(
            "No saved locations.\nTo save a location, tap the \"Save\" button on a location's information page.",
            textAlign: TextAlign.center
          )
        );
    }
  }
}