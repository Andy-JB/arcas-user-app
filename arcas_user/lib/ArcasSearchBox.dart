import 'package:arcas_user/SearchStateManager.dart';
import 'package:flutter/material.dart';

import 'SharedSearchFunctions.dart';
import 'SearchStateManager.dart';
import 'ArcasFilter.dart';

class SearchBox extends StatefulWidget {
  @override
  _SearchBoxState createState() => _SearchBoxState();
}

class _SearchBoxState extends State<SearchBox> {
  final ArcasSearchDelegate _searchDelegate = ArcasSearchDelegate();
  final SearchStateManager _searchStateManager = SearchStateManager();
  SearchButtonAppearance _searchButtonAppearance;

  void _showNoResultsFound() {
    // TODO: Implement message to show near search bar
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text('No search results found.')
    ));
  }

  Expanded _buildSearchFieldText() {
    return Expanded(
      child: _searchStateManager.query == null || _searchStateManager.query == ""
        ? PlaceholderText()
        : ResultText(context, _searchStateManager.query)
    );
  }

  void _changeState(SearchButtonAppearance searchButtonAppearance) {
    setState(() {
      _searchButtonAppearance = searchButtonAppearance;
      SearchState state = _searchStateManager.state;
      if(state == SearchState.noResults || state == SearchState.error) {
        _showNoResultsFound();
      }
    });
  }

  void _resetState() {
    _searchStateManager.resetState();
  }

  SearchButtonAppearance _determineState() {
    switch (_searchStateManager.state) {
      case SearchState.waiting:
        return SearchButtonAppearance.search;
      case SearchState.loading:
        return SearchButtonAppearance.loading;
      case SearchState.loaded:
      case SearchState.noResults:
      case SearchState.error:
        return SearchButtonAppearance.back;
      default:
        throw "Unimplemented 'SearchState' case";
    }
  }

  @override
  void setState(fn) {
    if(mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();
    _searchButtonAppearance = _determineState();
    _searchStateManager.addListener(() => _changeState(_determineState()));
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    Widget searchBox = GestureDetector(
      child: Container( // Search box appearance
        // Width is calculated like this so it looks good in landscape too
        width: screenSize.width - (screenSize.shortestSide * 0.1),
        height: 48,
        child: Material(
          elevation: 6,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
          type: MaterialType.card, // Uses theme's cardColor
          child: Row(
            children: [
              Padding(padding: EdgeInsets.only(left: 5.0)),
              SearchButton(
                appearance: _searchButtonAppearance,
                backOnTap: _resetState,
              ),
              _buildSearchFieldText(),
              IconButton(
                padding: EdgeInsets.only(left: 20.0),
                icon: Icon(
                  Icons.filter_list,
                  semanticLabel: "Filter"
                ),
                onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ArcasFilter()))
              ),
              Padding(padding: EdgeInsets.only(right: 15.0))
            ]
          ),
        )
      ),
      onTap: () { // Search box functionality
        showSearch(context: context, delegate: _searchDelegate, query: _searchStateManager.query);
      }
    );

    // This allows the user to clear search results using the device's back button
    if(_searchStateManager.state != SearchState.waiting) {
      return WillPopScope(
        onWillPop: () async {
          _resetState();
          return false;
        },
        child: searchBox
      );
    }
    return searchBox;
  }
}