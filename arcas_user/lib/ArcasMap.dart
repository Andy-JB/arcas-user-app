import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:mapbox_gl/mapbox_gl.dart' as Mapbox;
import 'package:geolocator/geolocator.dart'; // For getting the user's location
import 'package:url_launcher/url_launcher.dart';

import 'FirebaseHelper.dart';
import 'SearchStateManager.dart';
import 'ArcasSearchBox.dart';
import 'ArcasLocationPage.dart';
import 'ArcasGlobals.dart';

class ArcasMap extends StatefulWidget {
  static const String ACCESS_TOKEN =
      "pk.eyJ1IjoiYXBwbWFqaWsiLCJhIjoiY2s2MXA2aDVjMDM1ajNrbXgybHlqeHRiMCJ9.6Ooq3PUmAIKUNavNNk_98w"; // Public API Key

  @override
  _ArcasMapState createState() => _ArcasMapState();
}

class _ArcasMapState extends State<ArcasMap>
    with AutomaticKeepAliveClientMixin, WidgetsBindingObserver {
  final SearchStateManager _searchStateManager = SearchStateManager();
  final MarkerPopupController popupController = MarkerPopupController();
  Mapbox.MapboxMap _mapboxMap;
  Mapbox.MapboxMapController _mapController;
  Map<Mapbox.Symbol, LocationPoint> locationMap = Map();
  List listenerList = List();

  // For re-sizing and orientation issues
  double windowHeight;
  Orientation currentOrientation;

  // To help decide when we tap on the map, but not a marker
  bool popupLock = false;

  /*
  How the location info will be used.
  - Rendering the marker on the map
    - It should be constantly updating its position
  - Change the setting for user location to a switch like the dark mode toggle.
  - Display the distance between two LatLngs on the location information page
  */

  // The little popup to say if the user wants to activate tracking
  bool trackingPopup = usersFirstVisit;
  bool streamActive = false;

  // Handling the user's location
  Geolocator geolocator;

  // For using the user's location to visualise it
  Mapbox.LatLng userLatLng;
  Mapbox.CircleOptions userCircleOptions;
  Mapbox.Circle userCircleHandle;

  @override bool get wantKeepAlive => true;


  void _trackUser() async {
    if(streamActive){
      return;
    }

    // TODO Potentially check if GPS settings are enabled
    // bool isLocationServiceEnabled  = await Geolocator.isLocationServiceEnabled();
    // LocationPermission permission = await Geolocator.requestPermission();

    streamActive = true;
    Stream<Position> stream = Geolocator.getPositionStream()
      .handleError((onError) {
        // debugPrint('handled position error: $onError');
      });

    stream.listen((Position position) {
      // Variables for the CircleOptions
      double opacity;
      Mapbox.LatLng newLatLng;

      if(position == null){
        newLatLng = Mapbox.LatLng(0, 0);
        opacity = 0;
      }
      else {
        newLatLng = Mapbox.LatLng(position.latitude, position.longitude);
        opacity = 1;
      }

      // If coordinates changes, then update the dot
      if(userLatLng.longitude != newLatLng.longitude
          || userLatLng.latitude != newLatLng.latitude){
        // Update the circle position and opacity
        userLatLng = newLatLng;
        userCircleOptions = userCircleOptions.copyWith(Mapbox.CircleOptions(
          geometry: userLatLng,
          circleOpacity: opacity,
          circleStrokeOpacity: opacity
        ));

        // Since this code can run before the circle is created
        if(userCircleHandle != null) {
          _mapController.updateCircle(userCircleHandle, userCircleOptions);
        }
      }
    });
  }

  void _checkTrackUser() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if(permission != LocationPermission.denied
        && permission != LocationPermission.deniedForever) {
      _trackUser();
    }
  }

  // Is called when app is brought back into foreground
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    _checkTrackUser();
  }

  void _onStyleLoaded() async {
    //todo: fix code so it's more appropriate
    Map markerCreation = {"markerLow" : "LOW", "markerMedium" : "MEDIUM", "markerFull" : "FULL", "markerNA" : "N/A", "markerClosed" : "CLOSED"};
    for(MapEntry markerEntry in markerCreation.entries) {
      ByteData data = await rootBundle.load("assets/markers/${markerEntry.key}.png");
      await _mapController.addImage(markerEntry.value, data.buffer.asUint8List());
    }

    _mapController.onSymbolTapped.add((marker) {
      popupLock = true;

      // Change this to take a LocationPoint object based off marker
      popupController.showLocation(locationMap[marker]);
    });

    _searchStateManager.addListener(() {
      switch(_searchStateManager.state) {
        case SearchState.waiting:
        case SearchState.loading:
        case SearchState.loaded:
        case SearchState.noResults:
          popupController.hideLocation();
          popupLock = false;
          buildSymbols();
          break;

        case SearchState.error:
          break;
      }
    });

    // Build symbols if any were meant to be loaded before map was ready
    buildSymbols();

    // Setup the geolocator
    geolocator = Geolocator();

    // The userLatLng will be updated
    userLatLng = Mapbox.LatLng(0, 0);

    // Toggle the opacity on/off and userLatLng when "hiding" the circle
    // Currently its made as if its off
    userCircleOptions = Mapbox.CircleOptions(
      circleRadius: 8,
      circleColor: '#4286F5',
      circleStrokeColor: '#FFFFFF',
      circleStrokeWidth: 2,
      circleStrokeOpacity: 0,
      circleOpacity: 0,

      // "geometry" field is required, or else you'll get a silent exception
      geometry: userLatLng,
      draggable: false,
    );

    // Add the user circle to the map and grab a reference to its handle
    _mapController.addCircle(userCircleOptions).then((res) {
      userCircleHandle = res;
    });

    // So we can auto-update the app when permissions change
      // Calls "didChangeAppLifecycleState()" which is pretty much the code below this
    WidgetsBinding.instance.addObserver(this);

    _checkTrackUser();

  }
  
  void clearLocationMap() {
    locationMap.forEach((sym, location) { 
      location.removeListener(location.updateSymbol);
    });
    locationMap.clear();
  }

  //builds symbols on the map: All previous symbols and connections are cleared.
  void buildSymbols() async {
    clearLocationMap();
    await _mapController.clearSymbols();

    _searchStateManager.locationList.forEach((location) {
      _mapController.addSymbol(buildSymbolOptions(location)).then((sym) {
        location.symbol = sym;
        locationMap[location.symbol] = location;
        location.updateSymbol = () {
          if(_mapController.symbols.contains(location.symbol)) {
            _mapController.updateSymbol(location.symbol, buildSymbolOptions(location));
          }
        };
        location.addListener(location.updateSymbol);
      });
    });
  }

  Mapbox.SymbolOptions buildSymbolOptions(LocationPoint location) {
    Map numToCapacity = {0 : "LOW", 1 : "MEDIUM", 2 : "FULL", 3 : "N/A"};
    return Mapbox.SymbolOptions(
      geometry: Mapbox.LatLng(location.latLng["lat"], location.latLng["lng"]),
      iconImage: location.serviceOpen(DateTime.now()) ? numToCapacity[location.averageCapacity] : "CLOSED", //if the place is not open, it will be shown as CLOSED
      iconSize: (1.0 / 3)
    );
  }

  @override
  void initState() {
    super.initState();

    _mapboxMap = Mapbox.MapboxMap(
        accessToken: ArcasMap.ACCESS_TOKEN,
        onMapCreated: (controller) {
          _mapController = controller;
        },
        onMapClick: (_, __) {
          if (!popupLock) {
            popupController.hideLocation();
          }
          popupLock = false;
        },
        onStyleLoadedCallback: _onStyleLoaded,
        zoomGesturesEnabled: true,
        initialCameraPosition: Mapbox.CameraPosition(
          target: Mapbox.LatLng(-34.92866, 138.59863), // Adelaide
          zoom: 11.0
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    currentOrientation ??= MediaQuery.of(context).orientation;
    return OrientationBuilder(
        builder: (BuildContext context, Orientation orientation) {
      if (currentOrientation != orientation) {
        // Resets map dimensions if orientation is changed
        currentOrientation = orientation;
        windowHeight = null;
      }
      return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          MediaQueryData mediaQuery = MediaQuery.of(context);
          windowHeight ??= mediaQuery.size.height - kBottomNavigationBarHeight; // Assigns on first run, prevents map resizing (would cause low FPS)
          return SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            child: Container(
              height: windowHeight,
              child: Stack(children: <Widget>[
                _mapboxMap,
                Positioned(
                  // Top is calculated like this so it looks good in landscape too.
                  // 0.05 means 5% padding from the edge of the screen on both
                  // sides of the search bar.
                  top: mediaQuery.padding.top + (mediaQuery.size.shortestSide * 0.05),
                  left: mediaQuery.size.shortestSide * 0.05,
                  child: SearchBox()
                ),
                !trackingPopup
                  ? Container()  // Stack doesn't like null, so we'll give it an empty container
                  : Positioned(
                      // bottom: mediaQuery.size.shortestSide * 0.075,
                      bottom: 30,
                      left: mediaQuery.size.shortestSide * 0.05,
                      right: mediaQuery.size.shortestSide * 0.05,
                      child: Material(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                        type: MaterialType.card, // Uses theme's cardColor
                        elevation: 6,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(10, 3, 10, 3),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Show your location?'),
                                ],
                              ),
                              Column(
                                children: [
                                  Row(
                                    children: [
                                      MaterialButton(
                                        color: Theme.of(context).primaryColor,
                                        child: Text('No'),
                                        onPressed: (){
                                          setState(() {
                                            trackingPopup = false;
                                          });
                                          Scaffold.of(context).showSnackBar(SnackBar(
                                            content: Text(
                                              'Location tracking disabled. This can be changed in the settings menu.'
                                            )
                                          ));
                                        }
                                      ),
                                      Padding(  // To separate the two buttons
                                        padding: EdgeInsets.all(5.0),
                                      ),
                                      MaterialButton(
                                        color: Theme.of(context).buttonColor,
                                        child: Text('Yes',
                                          style: TextStyle(
                                            color: Colors.white,
                                          )
                                        ),
                                        onPressed: (){
                                          setState(() {
                                            trackingPopup = false;
                                          });
                                          _trackUser();
                                        }
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                  ),
                MarkerPopup(popupController),
              ])
            )
          );
      });
    });
  }
}

class MarkerPopup extends StatefulWidget {
  final controller;

  MarkerPopup(this.controller);

  @override
  _MarkerPopupState createState() => _MarkerPopupState(controller);
}

class _MarkerPopupState extends State<MarkerPopup>
    with SingleTickerProviderStateMixin {
  bool onScreen = false;

  // For getting the available time info
  DateTime currentTime;
  bool openStatus;
  bool closingSoon;

  // Empty locationData so it doesn't crash. You never see it
  LocationPoint location = LocationPoint.blank();

  _MarkerPopupState(controller) {
    controller.showLocation = showLocation;
    controller.hideLocation = hideLocation;
  }

  showLocation(LocationPoint newLocationData) {
    setState(() {
      location = newLocationData;
      currentTime = DateTime.now();
      openStatus = location.serviceOpen(currentTime);
      closingSoon = location.closesSoon(currentTime);

      onScreen = true;
    });
  }

  hideLocation() {
    setState(() {
      onScreen = false;
    });
  }

  @override
  void setState(fn) { // Prevents off-screen setState() error
    if(mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    Size screenSize = mediaQuery.size;
    final offset = screenSize.shortestSide * 0.025;
    Color buttonColor = Theme.of(context).accentColor;

    return AnimatedPositioned(
      duration: Duration(milliseconds: 500),
      bottom: onScreen ? offset : -(offset + (screenSize.height * 0.5)),
      // Hopefully half the screen is more than the actual widget size...
      left: offset,
      child: Container(
        width: screenSize.width - (2 * offset),
        padding: EdgeInsets.fromLTRB(
            screenSize.width * 0.05, 10, screenSize.width * 0.05, 10),
        decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          borderRadius: BorderRadius.circular(10), //The main box's edges. We want it to be mostly rectangular
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: location.isBlank ? [] : [ // If its off screen and we haven't chosen a marker, vars will be empty so we do this to avoid errors
            Text(
              location.name,
              style: Theme.of(context).primaryTextTheme.headline6,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
            SizedBox(height: 5),
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          location.address,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.left,
                          maxLines: 1,
                        ),
                        location.buildCategoriesList(),
                        location.buildOpeningTimes()
                      ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        height: 20,
                        child: Text(
                          "Directions",
                        )),
                      FittedBox(
                        child: Container(
                          padding: const EdgeInsets.all(0.0),
                          foregroundDecoration: BoxDecoration(
                            border: Border.all(
                              color: buttonColor,
                              width: 4,
                            ),
                            shape: BoxShape.circle,
                          ),
                          child: IconButton(
                            color: buttonColor,
                            splashColor: buttonColor,
                            iconSize: 32,
                            icon: Icon(Icons.directions),
                            onPressed: () async {
                              String url = "https://www.google.com/maps/search/?api=1&query=" + location.latLng['lat'].toString() + "," + location.latLng['lng'].toString();
                              if(await canLaunch(url)) {
                                await launch(url);
                              } else {
                                throw 'Could not launch $url';
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              width: (screenSize.width * 0.9) - 20, // 20 comes from the padding
              child: FlatButton(
                color: Theme.of(context).buttonColor,
                textColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
                onPressed: () {
                  Navigator.of(context).push(ArcasLocationPage.createRoute(location));
                },
                child: Text(
                  "View Information",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                  )
                )
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MarkerPopupController {
  void Function(LocationPoint) showLocation;
  void Function() hideLocation;

  MarkerPopupController();
}
