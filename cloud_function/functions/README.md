# INFO

This folder contains some scripts that developers can run to update the database.

### create_search_db.py

Pre-requisites:
- `pipe install --upgrade firebase-admin`
- `pip3 install inflect`
- `pip3 install nltk`
	- Then open a Python3 interpretter and run `nltk.download("wordnet")` once
- `python3 -m pip install git+https://github.com/clips/pattern.git@development`
	- pypi version is deprecated, master doesn't build, so ATM we have to install
	- from the development branch
	- However I can't get it to build. Keeps complaining about missing mysql files. Same thing if I build mysql

You also need to set the environment variable `GOOGLE_APPLICATION_CREDENTIALS` to point to the json file. some examples are below

- Linux/Mac: `GOOGLE_APPLICATION_CREDENTIALS="/mnt/c/Users/David/AndroidStudioProjects/arcas-user-app/cloud_function/functions/arcas-bd925-firebase-adminsdk-mq1vy-b33be29848.json"`

- Windows: `$env:GOOGLE_APPLICATION_CREDENTIALS="C:\Users\David\AndroidStudioProjects\arcas-user-app\cloud_function\functions\arcas-bd925-firebase-adminsdk-mq1vy-b33be29848.json"`

Finally you run it with `python3 create_search_db.py *--upload [t/f]`. If upload is set to true, then it will override the database values for the specific categories. If its false or not present then it will build the variables locally and output them to the terminal.

### index.js

This is the function that will handle the user's queries when they search for something. It interacts with the database

To re-deploy the function run `deploy.bat` on Windows (Or use a similar command for Linux)
