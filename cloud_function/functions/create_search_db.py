import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

import inflect	# For the plurals and singulars of words
from nltk.corpus import wordnet	# For synonyms
#MISSING#	# For different tenses
	# pattern3

inflect2 = inflect.engine()

def add_word(matches, word, categories):
	if word not in matches:
		matches[word] = set()

	for i in categories:
		matches[word].add(i)

def try_add(s, w, func):
	try:
		new = func(w)
		if new != False:	# If the word wasn't found
			s.add(new)
	except:
		pass

def add_match(matches, word, categories, variants_mode):
	if variants_mode < 0 or variants_mode > 1:
		print('INVALID "variants_mode" parameter')
		return

	word_list = word.lower().split()
	for w in word_list:
		# The current word is the key, and the values are the categories
			# If this function is called multiple times for the same 'w' then
			# it will only add the catagories that aren't already present
		if variants_mode == 0:
			add_word(matches, w, categories)
		else:	# For getting plurals
			s = set()

			s.add(w)
			try_add(s, w, inflect2.singular_noun)
			try_add(s, w, inflect2.plural)

			# We're getting synonyms aswell
			for syn in wordnet.synsets(w):
				for l in syn.lemmas():
					# Getting the actual words and replacing underscores with spaces
					s.add(l.name().replace('_', ' '))

			for i in s:
				add_word(matches, i, categories)

			# Different tense stuff should be supported, but pattern3 refuses to install
				# drink -> drinking/drank


def main(upload_mode):
	# from pattern3.en import conjugate, lemma, lexeme, PRESENT, SG
	# print(lemma('gave'))
	# print(lexeme('gave'))
	# print(conjugate(verb='give',tense=PRESENT,number=SG))

	if upload_mode == True:
		# Setting up authentication
		cred = credentials.ApplicationDefault()
		firebase_admin.initialize_app(cred)

		db = firestore.client()
		print('Firestore client created')

	categoryTranslate = dict();
	categoryTranslate['toilets'] = "Toilets";
	categoryTranslate['food'] = "Food";
	categoryTranslate['drink'] = "Drink";
	categoryTranslate['emergency accommodation'] = "EmergencyAccommodation";
	categoryTranslate['emergency relief'] = "EmergencyRelief";
	categoryTranslate['charging point'] = "ChargingPoint";
	categoryTranslate['transport'] = "Transport";
	categoryTranslate['long term accommodation'] = "LongTermAccommodation";
	categoryTranslate['shower'] = "Shower";
	categoryTranslate['health service'] = "HealthService";
	categoryTranslate['clothing'] = "Clothing";
	categoryTranslate['community center'] = "CommunityCenter";
	categoryTranslate['wifi'] = "WiFi";
	categoryTranslate['laundry'] = "Laundry";
	categoryTranslate['other'] = "Other";

	categories = list(categoryTranslate.keys());

	# Create our matches dict of strings to sets of strings
	matches = dict()

	print('Starting to add words')

	# Generate the matches for the categories (Except "other")
	for word in categories:
		variants_mode = 1
		if word == 'other':
			continue

		if (word == 'food' or word == 'emergency accomodation' or word == 'emergency relief' or
			word == 'long term accomodation' or word == 'wifi' or word == 'laundry'):
			variants_mode = 0

		word_as_list = list()
		word_as_list.append(word)
		add_match(matches, word, word_as_list, variants_mode)

	add_match(matches, 'pee', ['toilets'], 1)
	add_match(matches, 'poo', ['toilets'], 1)
	add_match(matches, 'urinal', ['toilets'], 1)
	add_match(matches, 'wash hand', ['toilets'], 1)

	add_match(matches, 'eat', ['food'], 1)	# also eats/eatting/ate
	add_match(matches, 'consume', ['food'], 1)
	add_match(matches, 'hungry', ['food'], 1)
	add_match(matches, 'edible', ['food'], 1)
	add_match(matches, 'snack', ['food'], 1)
	add_match(matches, 'breakfast', ['food'], 0)
	add_match(matches, 'lunch', ['food'], 0)
	add_match(matches, 'dinner', ['food'], 0)
	add_match(matches, 'fill', ['food'], 1)	# filling

	add_match(matches, 'soup', ['food', 'community center'], 1)
	add_match(matches, 'taste', ['food', 'drink'], 1)		# tasty
	add_match(matches, 'meal', ['food', 'drink'], 1)

	add_match(matches, 'thirst', ['drink'], 1)	# thirsty
	add_match(matches, 'liquid', ['drink'], 1)
	add_match(matches, 'water', ['drink'], 0)
	add_match(matches, 'hydrate', ['drink'], 1)	# hydration

	add_match(matches, 'refresh', ['drink', 'shower'], 1)	# refreshing

	add_match(matches, 'bed', ['emergency accomodation'], 1)		# beds/bedding
	add_match(matches, 'sleep', ['emergency accomodation'], 1)		# slept/sleeping
	add_match(matches, 'snooze', ['emergency accomodation'], 1)
	add_match(matches, 'short term', ['emergency accomodation'], 0)
	add_match(matches, 'stay', ['emergency accomodation'], 1)		# stays/staying
	add_match(matches, 'matress', ['emergency accomodation'], 1)	# matresses

	add_match(matches, 'accomodation', ['emergency accomodation', 'long term accomodation'], 1)

	add_match(matches, 'harm', ['emergency relief'], 0)
	add_match(matches, 'bleed', ['emergency relief'], 1)	# bleeding/bled
	add_match(matches, 'sick', ['emergency relief'], 0)
	add_match(matches, 'cut', ['emergency relief'], 1)
	
	add_match(matches, 'hospital', ['emergency relief', 'health service'], 1)
	add_match(matches, 'health', ['emergency relief', 'health service'], 1)		# healthy
	add_match(matches, 'hurt', ['emergency relief', 'health service'], 1)		# hurts/hurting
	add_match(matches, 'medical', ['emergency relief', 'health service'], 1)	# medicine

	add_match(matches, 'usb', ['charging point'], 0)
	add_match(matches, 'cable', ['charging point'], 1)
	add_match(matches, 'power', ['charging point'], 0)
	add_match(matches, 'charge', ['charging point'], 1)		# charges/charging
	add_match(matches, 'outlet', ['charging point'], 1)
	add_match(matches, 'electricity', ['charging point'], 0)

	add_match(matches, 'bus', ['transport'], 1)
	add_match(matches, 'car', ['transport'], 1)
	add_match(matches, 'bike', ['transport'], 1)
	add_match(matches, 'wheel', ['transport'], 1)
	add_match(matches, 'train', ['transport'], 1)
	add_match(matches, 'tram', ['transport'], 1)
	add_match(matches, 'taxi', ['transport'], 1)

	add_match(matches, 'home', ['long term accomodation'], 1)	# house
	add_match(matches, 'appartment', ['long term accomodation'], 1)
	add_match(matches, 'live', ['long term accomodation'], 1)	# lives/living
	add_match(matches, 'stay', ['long term accomodation'], 1)

	add_match(matches, 'wash body', ['shower'], 0)
	add_match(matches, 'clean', ['shower'], 1)	# cleanse
	add_match(matches, 'sponge', ['shower'], 1)
	add_match(matches, 'soap', ['shower'], 1)
	add_match(matches, 'shampoo', ['shower'], 1)
	add_match(matches, 'deodorant', ['shower'], 1)

	add_match(matches, 'pharmacy', ['health service'], 1)	# pharmasist
	add_match(matches, 'drug', ['health service'], 1)
	add_match(matches, 'sore', ['health service'], 0)
	add_match(matches, 'sick', ['health service'], 0)
	add_match(matches, 'tablet', ['health service'], 1)
	add_match(matches, 'emergency', ['health service'], 1)

	add_match(matches, 'wear', ['clothing'], 0)
	add_match(matches, 'blanket', ['clothing'], 1)
	add_match(matches, 'cloth', ['clothing'], 0)
	add_match(matches, 'warm', ['clothing'], 1)		# warmth/warms
	add_match(matches, 'beenie', ['clothing'], 1)
	add_match(matches, 'hat', ['clothing'], 1)
	add_match(matches, 't-shirt', ['clothing'], 1)	# t shirt
	add_match(matches, 'short', ['clothing'], 1)
	add_match(matches, 'pants', ['clothing'], 0)
	add_match(matches, 'sock', ['clothing'], 1)
	add_match(matches, 'underwear', ['clothing'], 0)
	add_match(matches, 'underpants', ['clothing'], 0)
	add_match(matches, 'jocks', ['clothing'], 0)
	add_match(matches, 'bra', ['clothing'], 1)
	add_match(matches, 'shirt', ['clothing'], 1)
	add_match(matches, 'dress', ['clothing'], 1)
	add_match(matches, 'skirt', ['clothing'], 1)
	add_match(matches, 'shoe', ['clothing'], 1)
	add_match(matches, 'glasses', ['clothing'], 0)

	add_match(matches, 'meet', ['community center'], 1)
	add_match(matches, 'greet', ['community center'], 1)
	add_match(matches, 'friend', ['community center'], 1)
	add_match(matches, 'talk', ['community center'], 1)		# talks/talking
	add_match(matches, 'chat', ['community center'], 1)
	add_match(matches, 'game', ['community center'], 1)
	add_match(matches, 'play', ['community center'], 1)		# plays/playing
	add_match(matches, 'playcenter', ['community center'], 1)
	add_match(matches, 'park', ['community center'], 1)		# parks/parking
	add_match(matches, 'course', ['community center'], 1)
	add_match(matches, 'class', ['community center'], 1)
	add_match(matches, 'church', ['community center'], 1)
	add_match(matches, 'worship', ['community center'], 1)
	add_match(matches, 'mosque', ['community center'], 1)
	add_match(matches, 'religion', ['community center'], 1)	# religious
	add_match(matches, 'library', ['community center'], 1)
	add_match(matches, 'gym', ['community center'], 1)

	add_match(matches, 'internet', ['wifi'], 1)
	add_match(matches, 'google', ['wifi'], 1)
	add_match(matches, 'search', ['wifi'], 1)	# searches/searching
	add_match(matches, 'signal', ['wifi'], 1)

	add_match(matches, 'wash clothes', ['laundry'], 0)
	add_match(matches, 'washing machine', ['laundry'], 1)
	add_match(matches, 'wash', ['laundry'], 1)				# washes/washing
	add_match(matches, 'dry', ['laundry'], 1)				# drys/dryer/drying
	add_match(matches, 'clothes', ['laundry'], 1)			# clothing
	add_match(matches, 'clean', ['laundry'], 1)				# cleans/cleaning
	add_match(matches, 'laundrette', ['laundry'], 1)
	add_match(matches, 'iron', ['laundry'], 1)				# ironer/ironing

	# Now we use input_list to check the listings from the categories
		# We check the title, description and location for matches
	print('Implement phase 2 when access to the database')
	#

	if upload_mode == True:
		print('Accessing the database')

		# Get the collections
		docref_word = db.collection(u'search-word-lookup')
		docref_category = db.collection(u'search-word-category-lookup')

		# Drop the old tables
		print('Deleting old data')
		docs = docref_word.stream()
		for doc in docs:
			doc.reference.delete()

		docs = docref_category.stream()
		for doc in docs:
			doc.reference.delete()

		# Create our tables
		print('Adding in the documents/entries')
		print('\tSearch Terms')
		for key, value in matches.items():
			docref_word.add({
				u'search_term': key,
				u'categories': firestore.ArrayUnion(sorted(value))
			})

		print('\tTrue Category Names')
		for key, value in categoryTranslate.items():
			docref_category.add({
				u'search_name': key,
				u'real_name': value
			})
	else:
		print('\nList of Search Terms: ')
		for key, value in matches.items():
			print(key + ": " + str(value))
		
		print('Length of Search Terms list: ' + str(len(matches)))

		print('\nList of True Categories: ')
		for key, value in categoryTranslate.items():
			print(key + ": " + str(value))

	print('--PROGRAM END--')


if __name__ == "__main__":
	import argparse

	parser = argparse.ArgumentParser(description = 'Generate the search query matches')
	parser.add_argument('-u', '--upload', action = 'store_true', help = 'Upload and replace the old database values')
	args = parser.parse_args()

	main(upload_mode = args.upload)
