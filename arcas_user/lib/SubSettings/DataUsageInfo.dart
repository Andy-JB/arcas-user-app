import 'package:flutter/material.dart';

class DataUsageInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Data Usage Information')
        ),
        body: ListView(
          children: <Widget>[
            Center(
              child: Container(
                padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
                child: Text('<Data Usage Information here>')
              )
            )
          ]
        )
      )
    );
  }
}

