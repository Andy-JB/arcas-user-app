import 'package:arcas_user/ArcasConstants.dart';
import 'package:arcas_user/SharedPrefs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:math';

import 'SearchStateManager.dart';
import 'ArcasConstants.dart';
import 'FirebaseHelper.dart';

FilterStorage filterStorage = FilterStorage();

enum MenuOption { changeTime, removeTime, cancel }

//TODO: Defined closed: Is a place closed when the location's hours are closed or when the service hours are closed? Both?
class FilterStorage {
  final Map capacityMap = {"LOW": 0, "MEDIUM": 1, "FULL" : 2, "NA" : 3};
  final List<int> filterPriority = [1,0,2];
  final allCapacity = 2;
  final int naValue = ArcasConstants.capacityNumber['NA'];
  final showNADefault = false;
  final showClosedDefault = true;
  List<String> filteredServices;
  bool filteringHours;
  //showNA and Closed both ignore capacity
  bool showNA;
  bool showClosed;
  //time = AM/PM format, formattedTime = 00:00 format, storedTime is DateTime format
  String time;
  String formattedTime;

  // 0 = medium, 1 = low, 2 = all
  int filteredCapacity;
  Map defaultOptions;

  FilterStorage() {
    //list of services that are filtered, blank = all services. Note that this list is copied by value.
    filteredServices = [];
    filteringHours = false;
    time = "N/A";
    formattedTime = "23:59";
    filteredCapacity = 0;
    showNA = showNADefault;
    showClosed = showClosedDefault;
    defaultOptions = {
      "filteredServices": List.from(filteredServices),
      "filteringHours": filteringHours,
      "time": time,
      "formattedTime": formattedTime,
      "filteredCapacity": filteredCapacity,
      "showNA": showNA,
      "showClosed": showClosed
    };
  }

  //creates a new default for the filter
  Map<String,dynamic> getNewDefault() {
    return {
      "filteredServices": List.from(filteredServices),
      "filteringHours": filteringHours,
      "time": time,
      "formattedTime": formattedTime,
      'filteredCapacity': filteredCapacity,
      "showNA": showNA,
      "showClosed": showClosed
    };
  }

  void setSettingsToDefault() {
    filteredServices = List.from(defaultOptions['filteredServices']);
    filteringHours = defaultOptions['filteringHours'];
    time = defaultOptions['time'];
    formattedTime = defaultOptions['formattedTime'];
    filteredCapacity = defaultOptions['filteredCapacity'];
    showNA = defaultOptions['showNA'];
    showClosed = defaultOptions['showClosed'];

  }

  List filterLocationList(List locationLists) {
    locationLists.removeWhere((location) {
      return filterLocation(location);
    });

    return locationLists;
  }

  // Checks if the location meets our filter requirements
  bool filterLocation(LocationPoint location) {
    bool closed = testClosed(location);
    if (testServices(location, closed) && (closed || (testLocationCapacity(location))) && testLocationHours(location)) {
      return true;
    }
    return false;
  }

  // Checks the individual service's capacity and see if it meets our filter requirements
  // (filteredCapacity == allCapacity) is when we are searching all categories
  bool testServiceCapacity(ArcasService service) {
    return
      (filteredCapacity == allCapacity && service.capacity != "NA")
        || filterPriority.indexOf(filteredCapacity) >= service.capacityNumber
        || (service.capacity == "NA" && showNA == true);
  }

  // If the main place is closed right now
  bool testClosed(LocationPoint location) {
    return showClosed && !location.serviceOpen(DateTime.now());
  }

  bool testAgeGenderCapacity(ArcasService service) {
    AgeOptions age = parseAge(prefs.getAge());
    if(age != AgeOptions.NA && !service.ages.contains(age) ) {
      return false;
    }
    GenderOptions gender = parseGender(prefs.getGender());
    if(gender != GenderOptions.NA && !service.genders.contains(gender) ) {
      return false;
    }
    return true;
  }

  // Checks if the locations minimum capacity is within our filtered option
  // EG miniCap == med (Even if one service might be high/na), we are searching for low, this returns false
  bool testLocationCapacity(LocationPoint location) {
    return (filteredCapacity == allCapacity && location.minimumCapacity != naValue) ||
        filterPriority.indexOf(filteredCapacity) >= location.minimumCapacity ||
        (location.averageCapacity == naValue && showNA == true);
  }

  bool testLocationHours(LocationPoint location) {
    if(showClosed == true && filteringHours == false) {
      return true;
    }
    return location.serviceOpen(filteringHours ? hourColonMinuteToDateTime(formattedTime, DateTime.now()) : DateTime.now());
  }

  // Check if the hours meet our filtered hour requirements
  bool testHours(ArcasService service) {
    if(showClosed == true && filteringHours == false) {
      return true;
    }
    OpeningHoursDay times = service.openingHours.day(ArcasConstants.date);
    if (times == null) {
      return false;
    }
    if (times.isOpen == false) {
      return false;
    }
    if (filteringHours == false) {
      return true;
    }

    DateTime currentTime = DateTime.now();
    DateTime ft = hourColonMinuteToDateTime(formattedTime, currentTime);
    for (OpeningHoursDayHours time in times.times) {
      if (hourColonMinuteToDateTime(time.startTime, currentTime)
              .isBefore(ft) &&
          hourColonMinuteToDateTime(time.endTime, currentTime).isAfter(ft)) {
        return true;
      }
    }
    return false;
  }

  // Checks if one of the location's services are within our filtered options
  // At least one service needs to match
  bool testServices(LocationPoint location, [bool isClosed = false]) {

    // Check if any of the services match our filter. Key=name, Value=service
    for (ArcasService service in location.serviceList) {
      // Check if the service category, hours and capacity all match. Empty filter lists only need an age/capacity hit
      if (testAgeGenderCapacity(service) && (filteredServices.isEmpty || (filteredServices.contains(service.category) && testHours(service) && (isClosed || testServiceCapacity(service))))) {
        return true;
      }
    }
    return false;
  }

  //loops through the services of a location to find the average and minimum capacity. Returns NA if it can't find either.
  void getAverageAndMinimumCapacity(LocationPoint location) {
    //Capacities are 0 = low, 1 = medium, 2 = high, 3 (no hits) = NA
    List<int> foundCapacities = [];
    for(ArcasService service in location.serviceList) {
      if(service.capacityNumber < naValue && (filteredServices.isEmpty
          || filteredServices.contains(service.category))) {
          foundCapacities.add(service.capacityNumber);
      }
    }
    //gets the average and rounds
    location.averageCapacity = foundCapacities.length == 0 ? naValue : (foundCapacities.reduce((a,b) => a + b) / foundCapacities.length.toDouble()).round();
    location.minimumCapacity = foundCapacities.length == 0 ? naValue : foundCapacities.reduce(min);
  }

}

class ArcasFilter extends StatefulWidget {
  @override
  _ArcasFilterState createState() => _ArcasFilterState();
}

class _ArcasFilterState extends State<ArcasFilter> {
  List<String> capacities = ["Busy", "Not Busy", "All"];
  List<Color> capacitiesColor = [
    Colors.yellow,
    Colors.lightGreen,
    Colors.lightBlueAccent,
  ];

  final ScrollController _controllerHorizontal = ScrollController();
  Color _accentColour; //initialised in didChangeDependencies()

  List<Widget> getFilterChips() {
    //add all case widget in front
    List<Widget> widgetList = [
      FilterChip(
        label: Text("All"),
        selected: filterStorage.filteredServices.length == 0,
        onSelected: (bool value) {
          setState(() {
            filterStorage.filteredServices = [];
          });
        })
    ];
    for (String serviceName in ArcasConstants.categoryPriority) {
      bool contained = filterStorage.filteredServices.contains(serviceName);
      widgetList.add(Padding(
        padding: const EdgeInsets.all(8.0),
        child: FilterChip(
          label: Text(serviceName, textAlign: TextAlign.center),
          showCheckmark: false,
          selected: contained,
          avatar: Icon(ArcasConstants.iconNames[serviceName][contained ? 0 : 1]),
          onSelected: (bool value) {
            setState(() {
              if (value) {
                filterStorage.filteredServices.add(serviceName);
              } else {
                filterStorage.filteredServices.remove(serviceName);
              }
            });
          }),
      ));
    }
    return widgetList;
  }

  Future filterDropOptions() async {
    return showDialog<MenuOption>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(title: Text("Select Option"), children: <Widget>[
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, MenuOption.changeTime);
              },
              child: Text("Edit Seen Open Hours")),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, MenuOption.removeTime);
              },
              child: Text("View All Hours")),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, MenuOption.cancel);
              },
              child: Text("Cancel"))
          ]);
        });
  }

  Future<void> _showModal(context) {
    Size size = MediaQuery.of(context).copyWith().size;
    DateTime date = filterStorage.filteringHours ? hourColonMinuteToDateTime(filterStorage.formattedTime, DateTime.now()) : DateTime.now();
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext content) {
          return Container(
            height: size.height / 3,
            child:  Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(1.0),
                    child: CupertinoDatePicker(
                    initialDateTime: date,
                    mode: CupertinoDatePickerMode.time,
                    onDateTimeChanged: (newDate) {
                      date = newDate;
                    },
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FlatButton(
                      child: Text("Reset"),
                      textColor: Colors.red,
                      shape: BeveledRectangleBorder(side: BorderSide(width: 0.5, color: Colors.grey)),
                      onPressed: () {
                        filterStorage.filteringHours = false;
                        Navigator.pop(context);}
                    ),
                    SizedBox(width: 5),
                    FlatButton(
                      child: Text("Set", style: TextStyle(fontWeight: FontWeight.bold)),
                      textColor: Colors.white,
                      shape: BeveledRectangleBorder(side: BorderSide(width: 0.5,color: Colors.grey)),
                      onPressed: () {
                        bool isPM = date.hour >= 12;
                        //this hour is in 12 hour time: date.hour is in 24 hour time. Minute is minute displayed with at least a 0 if below 10
                        int hour = date.hour % 12;
                        String minute =
                            "${date.minute < 10 ? "0" : ""}${date.minute}";
                        //0 hour = 12 am/pm
                        if (hour == 0) {
                          hour = 12;
                        }
                        filterStorage.formattedTime = "${(date.hour < 10 ? "0" : "")}${date.hour}:$minute";
                        filterStorage.time = "$hour:$minute ${isPM ? "PM" : "AM"}";
                        filterStorage.filteringHours = true;
                        Navigator.pop(context);}
                    )
                  ],
                ),
              ],
            ),
          );
        });
  }

  @override
  void didChangeDependencies() { //Updates on theme change
    super.didChangeDependencies();
    _accentColour = Theme.of(context).accentColor;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        //we can check map equality by converting both to strings and comparing the strings: Otherwise, we have to check the filteredServices
        //array deeply as its reference is always different
        if(filterStorage.defaultOptions.toString() != filterStorage.getNewDefault().toString()) {
          bool result = await showDialog(context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Are you sure you want to leave without applying your new filters?"),
              actions: [
                TextButton(
                  child: Text("Cancel"),
                  onPressed: () => Navigator.of(context).pop(false),
                ),
                TextButton(
                  child: Text("Don't Apply"),
                  onPressed: () => Navigator.of(context).pop(true),
                ),
                TextButton(
                  child: Text("Apply"),
                  onPressed: () {
                    filterStorage.defaultOptions = filterStorage.getNewDefault();
                    SearchStateManager().resetFilters();
                    Navigator.of(context).pop(true);
                  }
                )
              ]
            );
          });
          if(result == null || result == false) {
            return false;
          }
        }
        filterStorage.setSettingsToDefault();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Location Filters'),
          actions: <Widget>[
            FlatButton(
              child: Text("Reset Filters",
                style: TextStyle(color: Colors.red),
                textScaleFactor: 1.25
              ),
              onPressed: () {
                setState(() {
                  filterStorage.filteringHours = false;
                  filterStorage.filteredCapacity = 0;
                  filterStorage.filteredServices = [];
                  filterStorage.showNA = filterStorage.showNADefault;
                  filterStorage.defaultOptions = filterStorage.getNewDefault();
                });
              })
          ],
        ),
        body: Material(
          // Needed for the icons (Think ink needing paper to leave its mark)
          child: ListView(
            children: [
              Container(
                width: double.infinity,
                child: Card(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Open Hours",
                      style: TextStyle(
                        height: 2,
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: ActionChip(
                        avatar: Icon(Icons.timelapse),
                        label: Text(
                          filterStorage.filteringHours
                            ? "All Services Open At ${filterStorage.time}"
                            : "All Hours",
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: _accentColour
                          )
                        ),
                        labelPadding: EdgeInsets.all(3),
                        onPressed: () {
                          _showModal(context).then((value) {
                            setState(() {});
                          });
                        },
                      ),
                    )
                  ],
                )),
              ),
              Card(
                child: Column(
                  children: [
                    Text(
                      "Capacity",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        height: 2
                      )
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List<Widget>.generate(3, (int index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ChoiceChip(
                            label: Text(capacities[index]),
                            selected: filterStorage.filteredCapacity == index,
                            selectedColor: capacitiesColor[index],
                            selectedShadowColor: Colors.black,
                            labelStyle: TextStyle(color: Colors.black),
                            onSelected: (bool selected) {
                              setState(() {
                                filterStorage.filteredCapacity = index;
                              });
                            }),
                        );
                      })),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                            child: FilterChip(
                                label: Text("Show Closed"),
                                selected: filterStorage.showClosed || filterStorage.filteringHours, //if hours are being filtered, this is automatically on, similar with onSelected
                                onSelected: (bool selected) {
                                  setState(() => filterStorage.showClosed = selected || filterStorage.filteringHours);
                                }),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                            child: FilterChip(
                              label: Text("Show Unavailable"),
                              selected: filterStorage.showNA,
                              onSelected: (bool selected) {
                                setState(() => filterStorage.showNA = selected);
                    }),
                          ),
                        ],
                      )
                    )
                ]
              )),
              Card(
                child: Container(
                  height: 110,
                  child: Column(
                    children: [
                      Text("Categories",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          height: 2
                        )
                      ),
                      Expanded(
                        child: NotificationListener<ScrollNotification>(
                          onNotification: (notification) => true,
                          child: Scrollbar(
                            isAlwaysShown: true,
                            controller: _controllerHorizontal,
                            thickness: 4,
                            child: ListView(
                              padding: const EdgeInsets.all(8),
                              scrollDirection: Axis.horizontal,
                              controller: _controllerHorizontal,
                              children: getFilterChips()),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Card(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: 155,
                      height: 50,
                      child: OutlinedButton.icon(
                        icon: Icon(Icons.edit_outlined, color: Colors.white),
                        label: Text("Apply Filters", textScaleFactor: 1.1, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
                        style: OutlinedButton.styleFrom(
                          backgroundColor: _accentColour,
                          shape: StadiumBorder()
                        ),
                        onPressed: () {
                          filterStorage.defaultOptions = filterStorage.getNewDefault();
                          SearchStateManager().resetFilters();
                          Navigator.of(context).pop();
                        }
                      ),
                    ),
                  ),
                )
              )
            ],
          ),
        )),
    );
  }
}
